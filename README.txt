Forgot Email
--------------
Today every user have multiple E-mail ids So they forgotten which E-mail 
address registered in a Site.
This module helps to user can known which E-Mail Address they registered with 
a Site and then they can reset the password.
We have to get the data from user like Mobile No, User code etc.. Based on 
that field user can retrieve their E-Mail address.

Configuration
-------------
Forgot Email module is provided with a configuration page. This configuration 
page configure to alternate field of retrieve the Email address.
